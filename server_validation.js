//Server side data validation functions
//validates MRN by checking to make sure body.MRN is a number and contains 9 digits
function validateMRN(body) {

    if ( isNaN(body.MRN) === false || body.MRN.toString().length === 9 ) {
        return true;
    } else {
        return false;
    }
}


module.exports = {
    validateMRN: validateMRN,
}