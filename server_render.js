//Server Side Data Transformation Functions

//data for sebia history
function dataSpepMassage(sebiahistory) {
    let vkeysebiahistory = Object.keys(sebiahistory);
    let arraysebiahistory = [];
    for (let vvkeysebiahistory of vkeysebiahistory) {
        arraysebiahistory.push({
            key: vvkeysebiahistory,
            spepdata: sebiahistory[vvkeysebiahistory],
            array_sdata: convertCurveData(sebiahistory[vvkeysebiahistory].curva),
            marker_sdata: convertMarkerData(sebiahistory[vvkeysebiahistory].curva),
        });

    };
    return arraysebiahistory;
}

//curve data for immunotype
function dataMassageSebia_ID_MORE(data) {
    let vdata = Object.keys(data);
    let arrayofdata = [];
    for (let vvdata of vdata) {
        arrayofdata.push({
            key: vvdata,
            id: data[vvdata].id,
            curva: convertCurveData(data[vvdata].curva),
        });

    };
    return arrayofdata;
}
    
function convertCurveData(sebiaCurva) {
    let sdata = [];
    let curvestream1 =[];
    let processedcurve = [];
    if (sebiaCurva !== null) {
        processedcurve = sebiaCurva.replace(/\s+/g, '').match(/.{4}/g);
        for (let i=0; i<processedcurve.length; i++)
            {
                if (parseInt(processedcurve[i],16) < 10000)
                {
                    curvestream1.push(parseInt(processedcurve[i],16));
                } else {
                    curvestream1.push(parseInt(processedcurve[i],16) & 0x3FFF);
                    // console.log('start');
                    // console.log(i);
                    // console.log(parseInt(processedcurve[i],16) & 0x3FFF);
                    // console.log('end');
                }
            }
        for (let i=0; i<300; i++)
            {
                    sdata.push({a:300-i, b:curvestream1[i]});
            }
        return sdata;

    } else {
        return sdata;
    }

}

function convertMarkerData(sebiaCurva) {
    let markerstream1 =[];
    let processedcurve = [];
    if (sebiaCurva !== null) {
        processedcurve = sebiaCurva.replace(/\s+/g, '').match(/.{4}/g);
        for (let i=0; i<processedcurve.length; i++)
            {
                if (parseInt(processedcurve[i],16) < 10000)
                {
                } else {
                    let singlemarker =[];
                    singlemarker.push({a:300-i, b: parseInt(processedcurve[i],16) & 0x3FFF}, {a:300-i, b: 0});
                    markerstream1.push(singlemarker);

                }
            }
        return markerstream1;

    } else {
        return markerstream1;
    }

}

module.exports = {
    dataSpepMassage: dataSpepMassage,
    dataMassageSebia_ID_MORE: dataMassageSebia_ID_MORE,
    convertCurveData: convertCurveData
}