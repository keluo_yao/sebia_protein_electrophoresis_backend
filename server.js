const DEFAULT_PORT = 3000;
const cors = require('cors');
const axios = require('axios');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();

var server_render = require('./server_render');
var server_validation = require('./server_validation');
var server_epic = require('./server_epic');
// const querystring = require('querystring');


/* Load Express app middleware */
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

/* Serve the static files from React app */

app.use(express.static(path.join(__dirname, 'dist')));

app.get('/', (req,res) =>{
  res.sendFile(path.join(__dirname+'/dist/index.html'));
});

app.get('/get', (req, res) => {
  res.send('get successful')
});

app.post('/post', (req, res) => {
  res.send('post successful')
});

//connect to Epic REST API

app.post('/spep/api/v1/results', (req, res, next) => {

  if (server_validation.validateMRN(req.body)) {

    axios.all([
      axios({
        method: 'get',
        url: 'https://##############/Interconnect-PRD/wcf/Custom.Clinical.GeneratedServices/Patient.svc/rest_2013/Patient/Medications',
        params: {
                  patientId: req.body.MRN,
                  patientIdType: 'MRN',
                  userId: '####',
                  userIdType: '############',
                  profileView: '3',
                  numberDaysToIncludeDiscontinuedAndEndedOrders: '30',
        },
        headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': '##############################'
        }
      }),

      axios({
            method: 'post',
            url: 'https://#############/Interconnect-PRD/wcf/Custom.Clinical.GeneratedServices/Patient.svc/rest_2013/Patient/Information',
            headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': '################################'                  
            },
            data: {
                  "UserID":{
                                  "ID":"###########",
                                  "IDType":"############"
                  },
                  "PatientID" : {
                                  "ID" : req.body.MRN,
                                  "IDType" : "MRN"
                  }
            },

          }),

      axios({
            method: 'post',
            url: 'https://###############/Interconnect-PRD/wcf/Custom.Orders.GeneratedServices/Patient.svc/rest/Patient/Orders',
            params: {
              OrderType: 'LAB',
              FromDate: req.body.dateTimeStart,
              ToDate: req.body.dateTimeEnd,
            },
            headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': '#############################'                      
            },
            data: {
                  "UserID":{
                                  "ID":"#############",
                                  "IDType":"############"
                  },
                  "PatientID" : {
                                  "ID" : req.body.MRN,
                                  "IDType" : "MRN"
                  }
            },

          }),
        axios({
            method: 'post',
            url: 'https://################/Interconnect-PRD/wcf/Custom.Orders.GeneratedServices/Patient.svc/rest/Patient/Orders',
            params: {
              OrderType: 'RAD',
              FromDate: req.body.dateTimeStart,
              ToDate: req.body.dateTimeEnd,
            },
            headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': '###########################'                      
            },
            data: {
                  "UserID":{
                                  "ID":"##############",
                                  "IDType":"############"
                  },
                  "PatientID" : {
                                  "ID" : req.body.MRN,
                                  "IDType" : "MRN"
                  }
            },

          }),

      axios({
            method: 'post',
            url: 'https://###############/Interconnect-PRD/wcf/Custom.Clinical.GeneratedServices/Patient.svc/rest_2013/Patient/Notes',
            params: {
              FromDate: req.body.dateTimeStart,
              ToDate: req.body.dateTimeEnd,
            },
            headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': '#########################'    
            },
            data: {
                  "UserID":{
                                  "ID":"################",
                                  "IDType":"#############"
                  },
                  "PatientID" : {
                                  "ID" : req.body.MRN,
                                  "IDType" : "MRN"
                  }
            },

          })
      ]).then(axios.spread(function (response1, response2, response3, response4, response5){
                                res.json({
                                  error: false,
                                  message: 'success',
                                  results: {
                                    Medications2: server_epic.dataMedMassage(response1.data),
                                    Information2: server_epic.dataInfoMassage(response2.data),
                                    OrdersLab2: server_epic.dataOrdersLabMassage(response3.data),
                                    OrdersRad2: server_epic.dataOrdersRadsMassage(response4.data),
                                    Notes2: server_epic.dataOrdersNotesMassage(response5.data),
                                  }
                                });   
      }))
      .catch(function(error){
        console.log(error);
        res.json({
          error: true,
          message: 'failed',
          results: {
          }
        }); 
      });
    } else {
      res.json({
        error: true,
        message: 'failed MRN number validation on dashboard API',
        results: {
          
        }
      }); 
    }


  });

app.post('/spep/api/v1/notes', (req, res, next) => {

  axios.all([

    axios({
      method: 'post',
      url: 'https://#################/Interconnect-PRD/wcf/Custom.Clinical.GeneratedServices/Patient.svc/rest_2013/Patient/Note',
      params: {
        NoteID: req.body.noteID,
        OutputFormat: 'HTML',
      },
      headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': '#############################'    
      },
      data: {
            "UserID":{
                            "ID":"###########",
                            "IDType":"###########"
            },
            "PatientID" : {
                            "ID" : req.body.MRN,
                            "IDType" : "MRN"
            }
      },

    })

    ]).then(axios.spread(function (response6){
                              res.json({
                                error: false,
                                message: 'success',
                                results: {
                                  Note: response6.data,
                                }
                              });   
    }))
    .catch(function(error){
      console.log(error);
      res.json({
        error: true,
        message: 'failed',
        results: {
          Note: error
        }
      });
    });
  });

  app.post('/spep/api/v1/orderresults', (req, res, next) => {

    axios.all([
  
      axios({
        method: 'post',
        url: 'https://##############/Interconnect-PRD/wcf/Custom.Orders.GeneratedServices/Patient.svc/rest/Patient/Results',
        params: {
          OrderID: req.body.orderID,
        },
        headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': '####################################'    
        },
        data: {
              "UserID":{
                              "ID":"###########",
                              "IDType":"################"
              },
              "PatientID" : {
                              "ID" : req.body.MRN,
                              "IDType" : "MRN"
              }
        },
  
      })
  
      ]).then(axios.spread(function (response7){
                                res.json({
                                  error: false,
                                  message: 'success',
                                  results: {
                                    Note: response7.data,
                                  }
                                });   
      }))
      .catch(function(error){
        console.log(error);
        res.json({
          error: true,
          message: 'failed',
          results: {
            Note: error
          }
        });
      });
    });
    


const pgp = require('pg-promise')();


//connect to Sebia instrument

const connection = {
  user: '#########', //env var: PGUSER
  database: '###########', //env var: PGDATABASE
  password: '###########', //env var: PGPASSWORD
  host: '############', // Server hosting the postgres database
  port: 0000, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
}

const db = pgp(connection);

app.post('/spep/api/v1/SPEP', (req, res, next) => {
  db.any('SELECT * FROM public.anagrafica WHERE public.anagrafica.data_analisi BETWEEN $1 AND $2 AND public.anagrafica.programma = $3 ORDER BY seq ASC, programma ASC, data_analisi ASC LIMIT 500',[
    req.body.dateTimeRange[0],
    req.body.dateTimeRange[1],
    'J'
  ]).then(function(data) {
              res.json({
        error: false,
        message: 'success',
        results: {
          Sebia: server_render.dataSpepMassage(data),
          // Sebia: data,
        }
      });  


    })
    .catch(function(error) {
        console.log(error);
        res.json({
          error: true,
          message: 'failed',
          results: {
            Sebia: error,
          }
        });
    });
        
});

app.post('/spep/api/v1/SPEP_MRN', (req, res, next) => {
  
    db.any('SELECT * FROM public.anagrafica WHERE public.anagrafica.free1 = $1 ORDER BY data_analisi DESC, programma ASC, seq ASC LIMIT 500',[
      req.body.MRN_spep
    ]).then(function(data) {
                res.json({
          error: false,
          message: 'success',
          results: {
            Sebia: server_render.dataSpepMassage(data),
          }
        });
  
      })
      .catch(function(error) {
        console.log(error);
        res.json({
          error: true,
          message: 'failed',
          results: {
            Sebia: error,
          }
        });
      });
  });

  app.post('/spep/api/v1/SPEP_ID_MORE', (req, res, next) => {
    
  
    db.any('SELECT public.anagrafica.id, public.anagrafica.curva FROM public.anagrafica WHERE public.anagrafica.id LIKE $1 ORDER BY public.anagrafica.id ASC LIMIT 500',[
      req.body.ID_spep + '%'
    ]).then(function(data) {
                res.json({
          error: false,
          message: 'success',
          results: {
            Sebia: server_render.dataMassageSebia_ID_MORE(data),
          }
        });  
  
      })
      .catch(function(error) {
        console.log(error);
        res.json({
          error: true,
          message: 'failed',
          results: {
            Sebia: error,
          }
        });
      });
  });


//connect to dashboard database

const connection_datatable = {
  user: '########', //env var: PGUSER
  database: '#########', //env var: PGDATABASE
  password: '######', //env var: PGPASSWORD
  host: '########', // Server hosting the postgres database
  port: 0000, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
}


const dt = pgp(connection_datatable);

app.post('/spep/api/v1/commentview', (req, res, next) => {
  dt.any('SELECT * FROM public.comments WHERE public.comments.id LIKE $1 LIMIT 500',[
    req.body.id
  ]).then(function(data) {
              res.json({
        error: false,
        message: 'success',
        results: {
          Sebia: data,
        }
      });
    })
    .catch(function(error) {
        console.log(error);
        res.json({
          error: true,
          message: 'failed',
          results: {
            Sebia: error,
          }
        });
    });
        
});

app.post('/spep/api/v1/commentinsert', (req, res, next) => {
  dt.none('INSERT INTO public.comments(id, comment) VALUES($1, $2)',[
    req.body.id,
    req.body.comment
  ]).then(() => {
    console.log('it worked');
    res.json({
      error: false,
      message: 'success',
      results: {
        Sebia: 'insert successful'
      }});
    }).catch(error => {
    console.log('failed');
    console.log(error);
    res.json({
      error: false,
      message: 'failed',
      results: {
        Sebia: 'insert failed'
      }
    });
  });
});



app.get('/', (req, res) => {
  res.status(403)
    .end();
});

/* Express listener */
app.listen(DEFAULT_PORT, () => {
  console.log('express running on port ' + DEFAULT_PORT);
});


