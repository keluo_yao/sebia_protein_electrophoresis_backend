//Server side data transformation functions for epic data

function dataMedMassage (resp) {
    let vkey = Object.keys(resp.MedicationOrders);
    let arrayMed = [];
    for (let vvkey of vkey) {
        arrayMed.push({
            key: vvkey,
            Name: resp.MedicationOrders[vvkey].Name
        });

    };
return arrayMed;

}

function dataInfoMassage (resp) {
    let arrayInfo = [];
    arrayInfo.push({
        key: "0",
        FirstName: resp.PatientInformation.Name.FirstName,
        MiddleName: resp.PatientInformation.Name.MiddleName,
        LastName: resp.PatientInformation.Name.LastName,
        DOB: resp.PatientInformation.DateOfBirth,
        Gender: resp.PatientInformation.Gender.Title,
        MRN: resp.PatientInformation.MRN,
    });

return arrayInfo;

}

function dataOrdersLabMassage (resp) {
    let vkey3 = Object.keys(resp.Orders);
    let arrayOrdersLab = [];
    for (let vvkey3 of vkey3) {
        arrayOrdersLab.push({
            key: vvkey3,
            OrderID: resp.Orders[vvkey3].OrderID,
            OrderDate: resp.Orders[vvkey3].OrderDate,
            OrderDisplayName: resp.Orders[vvkey3].OrderDisplayName,
        });
    }

return arrayOrdersLab;

}

function dataOrdersRadsMassage (resp) {
    let vkey3 = Object.keys(resp.Orders);
    let arrayOrdersRad = [];
    for (let vvkey3 of vkey3) {
        arrayOrdersRad.push({
            key: vvkey3,
            OrderID: resp.Orders[vvkey3].OrderID,
            OrderDate: resp.Orders[vvkey3].OrderDate,
            OrderDisplayName: resp.Orders[vvkey3].OrderDisplayName,
        });
    }

return arrayOrdersRad;

}

function dataOrdersNotesMassage (resp) {
    let vkey3 = Object.keys(resp.Notes);
    let arrayOrdersNotes = [];
    for (let vvkey3 of vkey3) {
        arrayOrdersNotes.push({
            key: vvkey3,
            NoteId: resp.Notes[vvkey3].NoteId,
            DateOfService: resp.Notes[vvkey3].DateOfService,
            NoteType: resp.Notes[vvkey3].NoteType,
        });
    }

return arrayOrdersNotes;

}


module.exports = {
    dataMedMassage: dataMedMassage,
    dataInfoMassage: dataInfoMassage,
    dataOrdersLabMassage: dataOrdersLabMassage,
    dataOrdersRadsMassage: dataOrdersRadsMassage,
    dataOrdersNotesMassage: dataOrdersNotesMassage,
}